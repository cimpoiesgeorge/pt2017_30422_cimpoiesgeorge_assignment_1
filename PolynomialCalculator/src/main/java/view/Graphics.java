package view;

/**
 * Created by George Cimpoies on 3/7/2017.
 */

import model.Polynomial;
import utilities.ParsePolynomial;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class Graphics {

    public JFrame getFrame() {
        return frame;
    }

    private JFrame frame;
    private JTextField firstPolynom;
    private JTextField secondPolynom;
    private JTextField ans;

    /**
     * Create the application.
     */
    public Graphics() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 719, 470);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        firstPolynom = new JTextField();
        firstPolynom.setBounds(40, 45, 249, 35);
        frame.getContentPane().add(firstPolynom);
        firstPolynom.setColumns(10);

        secondPolynom = new JTextField();
        secondPolynom.setBounds(388, 45, 249, 35);
        frame.getContentPane().add(secondPolynom);
        secondPolynom.setColumns(10);

        JButton btnNewButton = new JButton("Add");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Polynomial p1 = ParsePolynomial.parseString(firstPolynom.getText());
                Polynomial p2 = ParsePolynomial.parseString(secondPolynom.getText());
                p1.sort();
                p2.sort();
                Polynomial result = p1.add(p2);
                result.sort();
                String answer = result.toString();

                ans.setText(answer);


            }
        });
        btnNewButton.setBounds(40, 144, 183, 75);
        frame.getContentPane().add(btnNewButton);

        JButton btnNewButton_1 = new JButton("Subtract");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Polynomial p1 = ParsePolynomial.parseString(firstPolynom.getText());
                Polynomial p2 = ParsePolynomial.parseString(secondPolynom.getText());
                p1.sort();
                p2.sort();
                Polynomial result = p1.subtract(p2);
                result.sort();
                String answer = result.toString();

                ans.setText(answer);
            }
        });
        btnNewButton_1.setBounds(254, 144, 183, 75);
        frame.getContentPane().add(btnNewButton_1);


        JButton btnNewButton_2 = new JButton("Differentiate \u2202");
        btnNewButton_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Polynomial p1 = ParsePolynomial.parseString(firstPolynom.getText());
                p1.sort();
                Polynomial result = p1.differentiate();
                result.sort();
                String answer = result.toString();

                ans.setText(answer);
            }
        });
        btnNewButton_2.setBounds(40, 232, 183, 75);
        frame.getContentPane().add(btnNewButton_2);

        JButton btnNewButton_3 = new JButton("Integrate \u222B");
        btnNewButton_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Polynomial p1 = ParsePolynomial.parseString(firstPolynom.getText());
                p1.sort();
                Polynomial result = p1.integrate();
                result.sort();
                String answer = result.toString();

                ans.setText(answer);
            }
        });
        btnNewButton_3.setBounds(469, 232, 183, 75);
        frame.getContentPane().add(btnNewButton_3);

        JButton btnNewButton_4 = new JButton("Multiply");
        btnNewButton_4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Polynomial p1 = ParsePolynomial.parseString(firstPolynom.getText());
                Polynomial p2 = ParsePolynomial.parseString(secondPolynom.getText());
                p1.sort();
                p2.sort();
                Polynomial result = p1.multiply(p2);
                result.sort();
                String answer = result.toString();

                ans.setText(answer);
            }
        });
        btnNewButton_4.setBounds(469, 144, 183, 75);
        frame.getContentPane().add(btnNewButton_4);

        JButton btnNewButton_5 = new JButton("Divide");
        btnNewButton_5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Polynomial p1 = ParsePolynomial.parseString(firstPolynom.getText());
                Polynomial p2 = ParsePolynomial.parseString(secondPolynom.getText());
                p1.sort();
                p2.sort();
                String result = p1.divide(p2);
                String answer = result;

                ans.setText(answer);
            }
        });
        btnNewButton_5.setBounds(254, 232, 183, 75);
        frame.getContentPane().add(btnNewButton_5);

        ans = new JTextField();
        ans.setBounds(130, 346, 426, 55);
        frame.getContentPane().add(ans);
        ans.setColumns(10);

        JLabel lblstPolynom = new JLabel("1st Polynom:");
        lblstPolynom.setFont(new Font("Adobe Arabic", Font.ITALIC, 30));
        lblstPolynom.setBounds(40, 13, 249, 29);
        frame.getContentPane().add(lblstPolynom);

        JLabel lblndPolynom = new JLabel("2nd Polynom:");
        lblndPolynom.setFont(new Font("Adobe Arabic", Font.ITALIC, 30));
        lblndPolynom.setBounds(388, 13, 249, 29);
        frame.getContentPane().add(lblndPolynom);

        JLabel lblNoteDerivationAnd = new JLabel("Note: DERIVATION and INTEGRATION is only applied on the first polynomial.");
        lblNoteDerivationAnd.setBounds(126, 72, 492, 75);
        frame.getContentPane().add(lblNoteDerivationAnd);

        JLabel lblAnswer = new JLabel("ANSWER:");
        lblAnswer.setFont(new Font("Adobe Arabic", Font.ITALIC, 30));
        lblAnswer.setBounds(254, 314, 193, 29);
        frame.getContentPane().add(lblAnswer);
    }
}