package controller;

import model.Polynomial;
import model.Term;

import java.awt.*;

/**
 * Created by George Cimpoies on 3/1/2017.
 */

public class Controller {

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    view.Graphics window = new view.Graphics();
                    window.getFrame().setVisible(true);
                    window.getFrame().setTitle("Polynomial Calculator - George Cimpoies");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
