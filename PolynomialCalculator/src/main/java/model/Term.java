package model;

/**
 * Created by George Cimpoies on 3/1/2017.
 */
public class Term {

    private double coefficient;
    private double degree;

    public Term(double coefficient, double degree) {
        this.coefficient = coefficient;
        this.degree = degree;
    }


    public double getCoefficient() {
        return coefficient;
    }

    void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public double getDegree() {
        return degree;
    }

    public void setDegree(double degree) {
        this.degree = degree;
    }

    @Override
    public String toString() {

        if (this.getCoefficient() == (int) this.getCoefficient()) {
            return (int) (this.getCoefficient()) + "x^" + (int) (this.getDegree());
        }
        return Math.floor(this.getCoefficient() * 100) / 100 + "x^" + this.getDegree();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Term)) {
            return false;
        }

        Term that = (Term) obj;

        return (that.getCoefficient() == this.getCoefficient()) && (that.getDegree() == this.getDegree());
    }
}
