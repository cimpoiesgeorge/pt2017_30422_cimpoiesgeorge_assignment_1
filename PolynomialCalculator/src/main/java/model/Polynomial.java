package model;

import utilities.TermComparator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by George Cimpoies on 3/1/2017.
 */

public class Polynomial {

    private ArrayList<Term> terms;

    public Polynomial() {
        this.terms = new ArrayList<Term>();
    }

    public Polynomial(Polynomial currentPolynomial) {
        this.terms = new ArrayList<Term>();
        for (Term term : currentPolynomial.getTerms()) {
            this.terms.add(new Term(term.getCoefficient(), term.getDegree()));
        }
    }

    public ArrayList<Term> getTerms() {
        return terms;
    }

    public void setTerms(ArrayList<Term> terms) {
        this.terms = terms;
    }

    public void sort() {
        Collections.sort(terms, new TermComparator());
    }

    public void addTerm(Term t) {
        boolean termExists = false;
        for (Term currentTerm : this.getTerms()) {
            if (currentTerm.getDegree() == t.getDegree()) {
                if (currentTerm.getCoefficient() + t.getCoefficient() == 0) {
                    this.getTerms().remove(currentTerm);
                } else {
                    currentTerm.setCoefficient(currentTerm.getCoefficient() + t.getCoefficient());
                }

                termExists = true;
                break;
            }
        }

        if (!termExists) {
            this.getTerms().add(new Term(t.getCoefficient(), t.getDegree()));
        }
    }

    public Polynomial add(Polynomial polynomial) {
        this.sort();
        polynomial.sort();
        Polynomial result = new Polynomial(this);
        for (Term term : polynomial.getTerms()) {
            result.addTerm(term);
        }
        return result;
    }

    public Polynomial subtract(Polynomial polynomial) {
        Polynomial result = new Polynomial(this);
        for (Term term : polynomial.getTerms()) {
            term.setCoefficient((-1) * term.getCoefficient());
            result.addTerm(term);
        }
        result.sort();
        return result;
    }


    public Polynomial multiply(Polynomial polynomial) {
        Polynomial result = new Polynomial();

        for (Term term1 : this.getTerms()) {
            double coeff1 = term1.getCoefficient();
            double degree1 = term1.getDegree();
            for (Term term2 : polynomial.getTerms()) {
                double coeff2 = term2.getCoefficient();
                double degree2 = term2.getDegree();
                Term t = new Term(coeff2 * coeff1, degree1 + degree2);
                result.addTerm(t);
            }
        }
        return result;
    }

    public String divide(Polynomial b) {

        Polynomial partialResult = new Polynomial();
        Polynomial remainder = new Polynomial(this);

        Term partialTerm;
        double partialCoefficient;
        double partialDegree;

        while ((remainder.getTerms().get(0).getDegree() > b.getTerms().get(0).getDegree())) {

            partialCoefficient = remainder.getTerms().get(0).getCoefficient() / b.getTerms().get(0).getCoefficient();
            partialDegree = remainder.getTerms().get(0).getDegree() - b.getTerms().get(0).getDegree();

            partialTerm = new Term(partialCoefficient, partialDegree);
            partialResult.getTerms().add(partialTerm);

            remainder = remainder.subtract(b.multiplyByTerm(partialTerm));

        }
        return "Quotient: "+partialResult.toString()+" Remainder: "+remainder.toString();
    }


    public Polynomial differentiate() {
        Polynomial result = new Polynomial();

        for (Term term : this.getTerms()) {
            double coefficient = term.getCoefficient();
            double degree = term.getDegree();
            if (degree - 1 >= 0) {
                Term t = new Term(coefficient * degree, degree - 1);
                result.addTerm(t);
            }
        }
        return result;
    }


    public Polynomial integrate() {
        Polynomial result = new Polynomial();

        for (Term term : this.getTerms()) {
            double coefficient = term.getCoefficient();
            double degree = term.getDegree();

            Term t = new Term(coefficient / (1 + degree), degree + 1);
            result.addTerm(t);
        }
        return result;
    }

    private Polynomial multiplyByTerm(Term t) {
        Polynomial result = new Polynomial(this);
        for (Term term : result.getTerms()) {
            term.setCoefficient(term.getCoefficient() * t.getCoefficient());
            term.setDegree(term.getDegree() + t.getDegree());
        }
        return result;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        if (this.getTerms().size() > 0) {

            for (Term term : this.terms) {
                if (term.getCoefficient() > 0) {
                    sb.append("+");
                }
                if (term.getCoefficient() == (int) term.getCoefficient()) {
                    sb.append((int) term.getCoefficient());
                } else {
                    sb.append(new DecimalFormat("##.#").format(term.getCoefficient()));
                }

                sb.append("x^");

                if (term.getDegree() == (int) term.getDegree()) {
                    sb.append((int) term.getDegree());
                } else {
                    sb.append(term.getDegree());
                }
            }
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        return obj instanceof Polynomial && ((Polynomial) obj).terms.equals(this.terms);
    }
}
