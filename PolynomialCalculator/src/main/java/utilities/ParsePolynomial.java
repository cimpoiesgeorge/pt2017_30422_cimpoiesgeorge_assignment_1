package utilities;

import model.Polynomial;
import model.Term;

/**
 * Created by George Cimpoies on 3/7/2017.
 */
public class ParsePolynomial {


    public static Polynomial parseString(String polynomialRead) {

        String newPolynomial;
        Polynomial p = new Polynomial();


        newPolynomial = polynomialRead.replaceAll("-", "+-").replaceAll(" ", "");

        String[] parts = newPolynomial.split("\\+");

        double coef[] = new double[100];
        int power[] = new int[100];


        for (int i = 0; i < parts.length; i++) {

            if (!parts[i].equals(""))
                if (!parts[i].contains("x")) {

                    coef[i] = Float.parseFloat(parts[i]);
                    power[i] = 0;
                } else if (!parts[i].contains("^")) {

                    if (parts[i].equals("x")) {
                        coef[i] = 1;
                        power[i] = 1;
                    } else if (parts[i].equals("-x")) {
                        coef[i] = -1;
                        power[i] = 1;
                    } else {
                        String[] qqq = parts[i].split("x");
                        coef[i] = Float.parseFloat(qqq[0]);
                        power[i] = 1;
                    }


                } else {
                    String[] monom = parts[i].split("x\\^");
                    power[i] = Integer.parseInt(monom[1]);
                    if (!monom[0].equals(""))
                        if (!monom[0].equals("-"))
                            coef[i] = Float.parseFloat(monom[0]);
                        else
                            coef[i] = -1;
                    else
                        coef[i] = 1;

                }


            if (!(coef[i] == 0 && power[i] == 0)) {
                Term t = new Term(coef[i], power[i]);
                p.addTerm(t);
            }
        }
        return p;

    }

}