package model;

import junit.framework.TestCase;

/**
 * Created by George Cimpoies on 3/7/2017.
 */

public class PolynomialTest extends TestCase {

    private Polynomial p1 = new Polynomial();
    private Polynomial p2 = new Polynomial();

    public void setup() {

        Term t11 = new Term(1, 4);
        Term t12 = new Term(-4, 2);
        Term t13 = new Term(3, 1);
        Term t21 = new Term(4, 3);
        Term t22 = new Term(10, 1);
        Term t23 = new Term(7, 12);

        p1.addTerm(t11);
        p1.addTerm(t12);
        p1.addTerm(t13);

        p2.addTerm(t21);
        p2.addTerm(t22);
        p2.addTerm(t23);
    }


    public void testAdd() throws Exception {

        setup();

        Polynomial result = p1.add(p2);
        result.sort();

        assertEquals("+7x^12+1x^4+4x^3-4x^2+13x^1", result.toString());
    }

    public void testSubtract() throws Exception {

        setup();

        Polynomial result = p1.subtract(p2);
        result.sort();

        assertEquals("-7x^12+1x^4-4x^3-4x^2-7x^1", result.toString());
    }

    public void testMultiply() throws Exception {

        setup();

        Polynomial result = p1.multiply(p2);
        result.sort();

        assertEquals("+7x^16-28x^14+21x^13+4x^7-6x^5+12x^4-40x^3+30x^2", result.toString());
    }

    public void testDifferentiate() throws Exception {

        setup();

        Polynomial result = p1.differentiate();
        result.sort();

        assertEquals("+4x^3-8x^1+3x^0", result.toString());
    }

    public void testIntegrate() throws Exception {

        setup();

        Polynomial result = p1.integrate();
        result.sort();

        assertEquals("+0.2x^5-1.3x^3+1.5x^2", result.toString());
    }

    /*public void testDivide() throws Exception {

        setup();

        Polynomial result = p1.divide(p2);
        result.sort();

        assertEquals("+0.2x^5-1.3x^3+1.5x^2", result.toString());

    }*/

    public void testToString() throws Exception {

        setup();

        String result = p1.toString();

        assertEquals("+1x^4-4x^2+3x^1", result);

    }

    public void testEquals() throws Exception {

        setup();

        assertEquals(false, p1.equals(p2));

    }

}